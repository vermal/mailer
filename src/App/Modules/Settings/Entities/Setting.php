<?php

namespace Vermal\Mailer\Modules\Settings\Entities;

/**
 * @ORM\Entity @ORM\Table(name="setting")
 * @ORM\HasLifecycleCallbacks
 **/
class Setting
{
    /** @ORM\Column(type="string", nullable=true) **/
    protected $imap_host;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $imap_port;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $imap_username;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $imap_password;
}
