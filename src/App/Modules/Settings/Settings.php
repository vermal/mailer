<?php

namespace Vermal\Mailer\Modules\Settings;

use Vermal\Form\Form;

class Settings
{

    /**
     * Script that is executed when LocalizationSettings constructor is fired
     */
    public function start()
    {
        $this->tabs();
        $this->index();
        $this->store();
        $this->form();
    }

    private function tabs()
    {
        add_filter('settings_tabs', function($tabs) {
            $tabs['imap_mailer'] = 'IMAP Mailer';
            return $tabs;
        });
    }

    /**
     * Index page
     */
    private function index()
    {
        add_filter('settings_fields', function($fields) {
            $fields['imap_mailer'] = [];
            $fields['imap_mailer'][] = 'imap_host';
            $fields['imap_mailer'][] = 'imap_port';
            $fields['imap_mailer'][] = 'imap_username';
            $fields['imap_mailer'][] = 'imap_password';
            return $fields;
        });
    }

    /**
     * Store settings
     */
    private function store()
    {
        add_filter('settings_store', function($settings, $post) {
            $settings->imap_host = $post->imap_host;
            $settings->imap_port = $post->imap_port;
            $settings->imap_username = $post->imap_username;
            $settings->imap_password = $post->imap_password;
            return $settings;
        });
    }

    /**
     * Customize form
     */
    private function form()
    {
        add_filter('settings_form', function($form) {
            /** @var Form $form */

            $form->addText('imap_host', 'Host')->setCol('col-lg-8');
            $form->addText('imap_port', 'Port')->setCol('col-lg-4');
            $form->addText('imap_username', 'Username');
            $form->addText('imap_password', 'Password');

            return $form;
        });
    }

}
