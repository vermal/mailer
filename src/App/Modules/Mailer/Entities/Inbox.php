<?php

namespace Vermal\Mailer\Modules\Mailer\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="mailer_inbox")
 * @ORM\HasLifecycleCallbacks
 **/
class Inbox extends Model
{
    const THREAD_PREFIX = 'VZN-AQE-';
    const SEEN = 'seen';
    const UNSEEN = 'unseen';
    const SENT = 'sent';

    /** @ORM\Column(type="string", nullable=true) **/
    protected $thread_id;

    /** @ORM\Column(type="string") **/
    protected $subject;

    /** @ORM\Column(type="string") **/
    protected $from_email;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $from_name;

    /** @ORM\Column(type="string") **/
    protected $to_email;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $to_name;

    /** @ORM\Column(type="text") **/
    protected $message;

    /** @ORM\Column(type="array", nullable=true) **/
    protected $attachements;

    /** @ORM\Column(type="string") **/
    protected $status;

    /** @ORM\Column(type="datetime") **/
    protected $date_received;

    /**
     * @ORM\OneToMany(targetEntity="\Inbox", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\Inbox", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * Inbox constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->children = new ArrayCollection();
        $this->date_received = new \DateTime();
    }

    /**
    * @param $child
    */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove children
     */
    public function removeAllChildren() {
        $this->children->clear();
    }

    /**
     * @param string $date
     * @throws \Exception
     */
    public function setDateReceived($date)
    {
        $this->date_received = new \DateTime($date);
    }

    /**
     * Get date received
     *
     * @param string $format
     * @return false|string
     */
    public function getDateReceived($format = 'd.m.Y H:i:s')
    {
        return date($format, $this->date_received->getTimestamp());
    }
}
