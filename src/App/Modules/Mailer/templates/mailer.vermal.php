@extends('layout')

@section('body')

    @if(isset($message))
        <div class="card shadow-lg m-b-30">
            <div class="card-body">
                {!! $message !!}
            </div>
        </div>
    @endif

    <div class="card shadow-lg m-b-30">
        <div class="card-body">
            {!! $form !!}
        </div>
    </div>

@endsection
