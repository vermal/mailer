<?php

namespace Vermal\Mailer\Modules\Mailer;

use Vermal\Database\Entity;
use Vermal\Mailer\Defaults\Controller;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Mailer\Defaults\Imap;
use Vermal\Mailer\Modules\Mailer\Entities\Inbox;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;


class Mailer extends Controller
{
    public $defaultViewPath = false;
    public static $statuses = [];

    public function __construct()
    {
        parent::__construct();
        // $this->requiredPermission('product', $this->CRUDAction);
        $this->addToBreadcrumb('mailer', 'admin.mailer.index');
        self::$statuses = [
            Inbox::SEEN => $this->_('mailer.seen'),
            Inbox::UNSEEN => $this->_('mailer.unseen'),
        ];
        do_action('Mailer');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->setDefaultViewPath();

        $data = Database::Model('Inbox')->where('i.parent', '=')
            ->order('i.date_received', 'DESC')
            ->order('i.status', 'DESC');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('status', 'Status')->setRenderer(function($inbox) {
            if ($inbox->status == Inbox::SENT) $badge = 'info';
            else if ($inbox->status == Inbox::SEEN) $badge = 'secondary';
            else $badge = 'danger';
            return '<span class="badge badge-' . $badge . ' badge-pill">' . $this->_('mailer.' . $inbox->status) . '</span>';
        });
        $datagrid->addColumn('to_email', $this->_('mailer.from'))->setRenderer(function($inbox) {
            return apply_filters('mailer_preview_from', $inbox->to_email, $inbox);
        });
        $datagrid->addColumn('subject', $this->_('mailer.subject'));
        $datagrid->addColumn('date_received', $this->_('mailer.received'))->setRenderer(function($inbox) {
            return $inbox->getDateReceived();
        });

        // Add actions
        $datagrid->addEdit(['admin.mailer.edit', ['id']], 'Zobraziť/Odpovedať');
        $datagrid->addDelete(['admin.mailer.destroy_', ['id']]);

        $datagrid->addFilter('status', self::$statuses, $this->_('mailer.status'), 'col-lg-2');
        $datagrid->addSearch('subject', $this->_('mailer.subject'), 'col-lg-3');
        $datagrid->addSearch('message', $this->_('mailer.message'), 'col-lg-3');

        apply_filters('mailer_datagrid', $datagrid);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('mailer', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.mailer.create');
        }

        $mailer = parent::mailer();
        Mail::sendMail($mailer, $post->to_email, $post->to_name, $post->subject, $post->message);

        // Redirect with message
        $this->alert($this->_('mailer.created'));
        Router::redirect('admin.mailer.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.mailer.update', ['id' => $id]));

        /** @var Inbox $inbox */
        $inbox = Database::Model('Inbox')->find($id);
        if ($inbox->status !== Inbox::SENT) {
            $inbox->status = Inbox::SEEN;
            Database::saveAndFlush($inbox);
        }

        $form->setValues($inbox, ['subject', 'message']);

        if (strpos($inbox->subject, Inbox::THREAD_PREFIX) === false) {
            $inbox->thread_id = Mail::createThreadID($inbox);
            $inbox->subject = str_replace('Re: ', '', $inbox->subject);
            $inbox->subject = 'Re: ' . $inbox->subject . ' ' . $inbox->thread_id;
        }
        $form->getComponent('subject')->setValue($inbox->subject);
        $form->getComponent('thread_id')->setValue($inbox->thread_id);

        View::view('mailer', [
            "form" => $form->build(),
            "message" => $inbox->message
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.mailer.edit', ['id' => $id]);
        }

        $inbox = Database::Model('Inbox')->find($id);

        $mailer = parent::mailer();
        $inbox = Mail::sendMail($mailer, $post->to_email, $post->to_name, $post->subject, $this->thread($post->message, $inbox->message), $post->thread_id, $id);

        // Redirect with message
        $this->alert($this->_('mailer.created'));
        Router::redirect('admin.mailer.edit', ['id' => $inbox->id]);
    }

    /**
     * Make thread
     *
     * @param $new
     * @param $old
     * @return string
     */
    public function thread($new, $old)
    {
        $output = $new;
        $output .= '<div style="margin-top: 10px; border-left: 2px solid #000; padding-left: 5px;">';
            $output .= $old;
        $output .= '</div>';
        return $output;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /** @var Inbox $inbox */
        $inbox = Database::Model('Inbox')->find($id);
        if ($inbox !== null) {
            $inbox->removeAllChildren();
            Database::saveAndFlush($inbox);
        }
        Database::Model('Inbox')->delete($id);

        // Redirect
        $this->alert($this->_('mailer.deleted'));
        Router::redirect('admin.mailer.index');
    }
    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('send-mail', routerLink('admin.mailer.store'));

        // Default
        $form->addHidden('thread_id', '');
        $form->addText('to_email', $this->_('mailer.to'))->email()->setCol('col-lg-6');
        $form->addText('to_name', $this->_('mailer.to_name'))->required()->setCol('col-lg-6');
        $form->addText('subject', $this->_('mailer.subject'));
        $form->addTextEditor('message', $this->_('mailer.message'))->required();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
