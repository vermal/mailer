<?php

namespace Vermal\Mailer\Modules\Mailer;


use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Mailer\Defaults\Controller;
use Vermal\Mailer\Modules\Mailer\Entities\Inbox;

class Mail
{

    /**
     * Send new message or reply to existing message
     *
     * @param \Vermal\Admin\Defaults\Mailer $mailer
     * @param $to
     * @param $name
     * @param $subject
     * @param $body
     * @param $thread_id
     * @param null $child
     * @throws \PHPMailer\PHPMailer\Exception
     * @return Inbox
     */
    public static function sendMail(\Vermal\Admin\Defaults\Mailer $mailer, $to, $name, $subject, $body, $thread_id = null, $child = null)
    {
        /** @var Inbox $inbox */
        $inbox = Entity::getEntity('Inbox');;
        if ($child !== null) {
            $child = Database::Model('Inbox')->find($child);
            $child->parent = $inbox;
            $inbox->addChild($child);
            Database::save($child);
        }

        $inbox->subject = $subject;
        $inbox->from_email = $mailer->getFrom();
        $inbox->to_email = $to;
        $inbox->to_name = $name;
        $inbox->message = $body;
        $inbox->status = Inbox::SENT;
        $inbox->thread_id = $thread_id;

        // Allow others to modify object
        apply_filters('mailer_save_mail', $inbox);

        $inbox = Database::saveAndFlush($inbox);

        // Create subject if child is null --> new message
        if ($child === null) {
            self::createSubject($inbox);
        }
        $inbox->removeAllChildren();
        $inbox = Database::saveAndFlush($inbox);

        // Setup data
        $mailer->addAddress($to, $name);
        $mailer->subject($inbox->subject);
        $mailer->body($body);

        $mailer->send();
        return $inbox;
    }

    /**
     * Create threadID
     *
     * @param Inbox $inbox
     * @return string
     */
    public static function createThreadID($inbox)
    {
        return '[' . Inbox::THREAD_PREFIX . str_pad($inbox->id, 4, 0, STR_PAD_LEFT) . ']';
    }

    /**
     * Create subject
     *
     * @param $inbox
     * @param bool $new
     */
    public static function createSubject(&$inbox, $new = true)
    {
        $inbox->thread_id = self::createThreadID($inbox);
        $inbox->subject = ($new ? '' : 'Re: ') . $inbox->subject . ' ' . $inbox->thread_id;
    }

}
