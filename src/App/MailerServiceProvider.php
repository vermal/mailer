<?php

namespace Vermal\Mailer;

use Vermal\Admin\Defaults\Controller;
use Vermal\Console\Console;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Mailer\Defaults\Imap;
use Vermal\Mailer\Modules\Mailer\Entities\Inbox;
use Vermal\Mailer\Modules\Settings\Entities\Setting;
use Vermal\Mailer\Modules\Settings\Settings;
use Vermal\Router;
use Vermal\App;

/**
 * Class EcommerceServiceProvider
 * @package Vermal\ecommerce
 */
class MailerServiceProvider
{

    /**
     * Init admin
     */
    public function boot()
    {
       // Register entities
        $this->registerEntities([
            // Settings
            "Setting" => [
                "namespace" => Setting::class,
                'path' => "Modules/Settings/Entities"
            ],

            // Mailer
            "Inbox" => [
                "namespace" => Inbox::class,
                'path' => "Modules/Mailer/Entities"
            ]
        ]);

		// Add localization file
		App::merge('localization-files', [
			__DIR__ . '/../public/lang/'
		]);

        // Register routes
        $this->routes();

        // Actions
        add_action('Settings', [new Settings(), 'start']);

        // Import data from mail
        $this->import();
    }

    /**
     * Define routes
     */
    public function routes()
    {
        Router::prefix('{locale::locale}?/admin/', function() {

            // Default namespace for all modules
            $namespace = '\\Vermal\\Mailer\\Modules\\';

            Router::resource('mailer', $namespace . 'Mailer\\Mailer', 'mailer.');


        }, 'admin.', ['locale']);
    }

    /**
     * Register all entites and paths mentioned in boot method
     *
     * @param $entites
     */
    private function registerEntities($entites)
    {
        $database = App::get('database');
        foreach ($entites as $key => $entity) {
            $database['entities'][$entity['namespace']] = $key;
            $database['paths'][$key] = 'vendor/vermal/mailer/src/' . $entity['path'];
        }
        App::bind('database', $database);
    }

    /**
     * Import mailer inbox
     */
    private function import()
    {
        Console::registerCommand('imap-import', function() {
            // Settings
            $settings = Database::Model('Setting')->find(Controller::SETTINGS);

            // New Connection
            $email = new Imap();
            $connect = $email->connect(
                '{' . $settings->imap_host . ':' . $settings->imap_port . '/notls}INBOX',
                $settings->imap_username,
                $settings->imap_password);

            // If connected
            if ($connect) {
                // Get messages
                $messages = $email->getMessages('html');
                if (!empty($messages['data'])) {
                    $messages = $messages['data'];
                    foreach ($messages as $message) {
                        $this->saveMessage($message);
                    }
                    Database::flush();
                }
            }
        });
    }

    /**
     * Store message
     *
     * @param $message
     * @throws \Exception
     */
    private function saveMessage($message)
    {
        /** @var Inbox $inbox */
        $inbox = Entity::getEntity('Inbox');

        $inbox->subject = $message['subject'];
        $inbox->to_email = $message['from']['address'];
        $inbox->to_name = $message['from']['name'];
        $inbox->from_email = $message['to'];
        $inbox->message = $message['message'];
        $inbox->status = Inbox::UNSEEN;
        $inbox->setDateReceived(date('Y-m-d H:i:s', strtotime($message['date'])));

        if ($thread_id = $this->setChild($message['subject'])) {
            $thread = Database::Model('Inbox')
                ->where('i.thread_id', $thread_id)
                ->where('i.parent', '=')
                ->first();
            if (!empty($thread)) {
                $inbox->thread_id = $thread->thread_id;
                $thread->parent = $inbox;
                $inbox->addChild($thread);
                Database::save($thread);
            }
        }

        // Allow others to modify object
        apply_filters('mailer_save_mail', $inbox);

        Database::save($inbox);
    }

    /**
     * @param $subject
     * @return string|boolean
     */
    private function setChild($subject)
    {
        if (strpos($subject, '[' . Inbox::THREAD_PREFIX) !== false) {
            preg_match('/Re\:.*(\[' . Inbox::THREAD_PREFIX .  '.*])/', $subject, $m);
            if (!empty($m)) {
                return $m[1];
            }
        }
        return false;
    }

}
