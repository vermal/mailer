<?php

namespace Vermal\Mailer\Defaults;


use Vermal\Admin\Defaults\Mailer;
use Vermal\Admin\View;
use Vermal\Config\Application;

class Controller extends \Vermal\Admin\Defaults\Controller
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Set propper paths for View
        if (isset($this->defaultViewPath) && !$this->defaultViewPath) {
            $this->setViewPath();
        }

        parent::$langDataStatic = array_merge_recursive(parent::$langDataStatic, parent::getLocalizationFile(__DIR__ . '/../../public/lang/'));
    }

    /**
     * New view path for ecommerce modules
     */
    public function setViewPath()
    {
        $currentModule = str_replace('\\', '/', get_called_class());
        $currentModule = substr($currentModule, 0, strrpos( $currentModule, '/'));
        $currentModule = str_replace('Vermal/Mailer/Modules/', '', $currentModule);
        $currentModule = str_replace('App/Admin/Modules/', '', $currentModule);
        View::setModulePath($currentModule);
        View::setViewPath(__DIR__ . '/../Modules/' . $currentModule . '/templates/');
    }

}
